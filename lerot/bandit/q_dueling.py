from abstract_duel import AbstractDuel

import numpy as np
import logging
import argparse


def my_argmax(ay):
    idx = np.nonzero(ay == ay.max())[0]
    return idx[np.random.randint(0, idx.shape[0])]


class QDueling(AbstractDuel):
    def __init__(self, arms, arg=""):
        super(QDueling, self).__init__()
        self.arms = arms
        parser = argparse.ArgumentParser(prog=self.__class__.__name__)
        parser.add_argument("--sampler", type=str, default='QDueling')
        parser.add_argument("--alpha", type=float, default=0.51)
        parser.add_argument('--iterations', type=int, default=10**7)
        parser.add_argument("--continue_sampling_experiment", type=str,
                            default="No")
        parser.add_argument("--old_output_dir", type=str, default="")
        parser.add_argument("--old_output_prefix", type=str, default="")
        args = parser.parse_known_args(arg.split())[0]

        self.alpha = args.alpha
        self.n_arms = len(arms)
        self.w = np.ones((self.n_arms, self.n_arms))
        self.times = 2.0 * np.ones((self.n_arms, self.n_arms))
        self.ucb_l = 0.5
        self.ucb_r = 0.5
        self.t = 1
        self.arm_l = 0
        self.arm_r = 1
        self.iterations = args.iterations
        # self.limit = np.ceil(self.alpha * np.log(self.iterations)  / 25.0 * 10**6)
        self.limit = 10**5

    def get_ucb(self, arm_l, arm_r):
        c = np.sqrt(self.alpha * np.log(self.t) / self.times[arm_l, arm_r])
        m = self.w[arm_l, arm_r] / self.times[arm_l, arm_r]
        self.ucb_l =  m + c
        self.ucb_r = 1 - m + c

    def get_arms(self):
        if self.arm_l == self.arm_r:
            return self.arm_l, self.arm_r

        if self.limit < self.times[self.arm_l, self.arm_r]:
            self.arm_l = self.arm_r + 1
            self.arm_r += 2

        if self.arm_r >= self.n_arms:
            self.arm_l = self.n_arms - 1
            self.arm_r = self.arm_l

        self.get_ucb(self.arm_l, self.arm_r)
        if self.ucb_l < 0.5:
            self.arm_l = self.arm_r
            self.arm_r += 1
        elif self.ucb_r < 0.5:
            self.arm_r += 1

        if self.arm_r == self.n_arms:
            self.arm_r = self.arm_l
        return self.arm_l, self.arm_r

    def update_scores(self, winner, loser):
        self.times[self.arm_l, self.arm_r] += 1
        self.times[self.arm_r, self.arm_l] += 1
        if winner == loser:
                self.w[winner] += 1
        elif winner == self.arm_l:
            self.w[winner, loser] += 1
        else:
            self.w[loser, winner] += 1
        self.t += 1
        if self.t % 100000 == 0:
            logging.info('Q_dueling, iteration: %d, potential winner is %d.' %(self.t, self.get_winner()))

    def get_winner(self):
        m = self.w[self.arm_l, self.arm_r] / self.times[self.arm_l, self.arm_r]
        return self.arm_l if m>=0.5 else self.arm_r