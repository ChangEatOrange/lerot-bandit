from abstract_duel import AbstractDuel

import numpy as np
import logging
import argparse

def my_argmax(a):
    idx = np.nonzero(a == a.max())[0]
    return idx[np.random.randint(0, len(idx))]


class SelfSparring(AbstractDuel):
    def __init__(self, arms, arg=""):
        super(SelfSparring, self).__init__()
        self.arms = arms
        parser = argparse.ArgumentParser(prog=self.__class__.__name__)
        parser.add_argument("--sampler", type=str, default='Sparring')
        parser.add_argument("--delta", type=float, default=3.0)
        parser.add_argument("--m", type=int, default=2)
        parser.add_argument("--alpha", type=float, default=0.51)
        parser.add_argument("--continue_sampling_experiment", type=str,
                            default="No")
        parser.add_argument("--old_output_dir", type=str, default="")
        parser.add_argument("--old_output_prefix", type=str, default="")
        args = parser.parse_known_args(arg.split())[0]

        self.alpha = args.alpha
        self.n_arms = len(arms)
        self.w = np.ones(self.n_arms)
        self.l = np.ones(self.n_arms)
        self.theta = 0.5 * self.l
        self.t = 1
        self.iteration=0
        self.m = args.m
        self.delta = args.delta
    
    def ts_sample(self):
        self.theta = np.random.beta(self.w, self.l)
        return my_argmax(self.theta)
        
    def get_arms(self):        
        arm_c = self.ts_sample()
        arm_d = self.ts_sample()
        return arm_c, arm_d

    def update_scores(self, winner, loser):
        if self.t % 100000 == 0:
            logging.info('SelfSparring, iteration: %d, winner: %d' % (self.t, self.get_winner()))
        self.w[winner] += 1
        self.l[loser] += 1
        self.t += 1
        self.iteration += 1
        
    def get_winner(self):
        stat_win = (self.w / (self.w + self.l) > 0.5)
        return np.argmax(stat_win)

  